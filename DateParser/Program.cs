﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateParser
{
    class Program
    {
        
        
        public static DateTime DateParse(string DateEntry)
        {
            int year, month, day;
            
            char[] delimiterChars = {'/'};
            string[] dateparts = DateEntry.Split(delimiterChars, System.StringSplitOptions.RemoveEmptyEntries);
            
            month = Int32.Parse(dateparts[0]);
            day = Int32.Parse(dateparts[1]);
            year = Int32.Parse(dateparts[2]);
            
            if (dateparts[0].Length != 2)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (dateparts[1].Length != 2)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (dateparts[2].Length != 4)
            {
                throw new ArgumentOutOfRangeException();
            }
            
            
            DateTime result = new DateTime(year, month, day);
            return result;


           
        }

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("01/01/0001 : Expected Result? " + (DateParse("01/01/0001").ToString() == "1/1/0001 12:00:00 AM") + " Test Passed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/0001 : Test Failed");
            }
            try
            {
                Console.WriteLine("1/01/0001 : Expected Result? " + (DateParse("1/01/0001").ToString() == "1/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("1/01/0001 : Test Passed");
            }
            try
            {
                Console.WriteLine("01/1/0001 : Expected Result? " + (DateParse("01/1/0001").ToString() == "1/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/0001 : Test Passed");
            }
            try
            {
                Console.WriteLine("01/01/1 : Expected Result? " + (DateParse("01/01/1").ToString() == "1/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/1 : Test Passed");
            } try
            {
                Console.WriteLine("12/01/0001 : Expected Result? " + (DateParse("12/01/0001").ToString() == "12/1/0001 12:00:00 AM") + " Test Passed");
            }
            catch (Exception)
            {
                Console.WriteLine("12/01/0001 : Test Failed");
            } try
            {
                Console.WriteLine("01/31/0001 : Expected Result? " + (DateParse("01/31/0001").ToString() == "1/31/0001 12:00:00 AM") + " Test Passed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/31/0001 : Test Failed");
            } try
            {
                Console.WriteLine("01/01/9999 : Expected Result? " + (DateParse("01/01/9999").ToString() == "1/1/9999 12:00:00 AM") + " Test Passed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/9999 : Test Failed");
            } try
            {
                Console.WriteLine("00/01/0001 : Expected Result? " + (DateParse("00/01/0001").ToString() == "0/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("00/01/0001 : Test Passed");
            } try
            {
                Console.WriteLine("01/00/0001 : Expected Result? " + (DateParse("01/00/0001").ToString() == "1/0/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/00/0001 : Test Passed");
            } try
            {
                Console.WriteLine("01/01/0000 : Expected Result? " + (DateParse("01/01/0000").ToString() == "1/1/0000 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/01/0000 : Test Passed");
            } try
            {
                Console.WriteLine("13/01/0001 : Expected Result? " + (DateParse("13/01/0001").ToString() == "13/1/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("13/01/0001 : Test Passed");
            } try
            {
                Console.WriteLine("01/32/0001 : Expected Result? " + (DateParse("01/32/0001").ToString() == "1/32/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("01/32/0001 : Test Passed");
            } try
            {
                Console.WriteLine("02/29/0001 : Expected Result? " + (DateParse("02/29/0001").ToString() == "2/29/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("02/29/0001 : Test Passed");
            } try
            {
                Console.WriteLine("04/31/0001 : Expected Result? " + (DateParse("04/31/0001").ToString() == "4/31/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("04/31/0001 : Test Passed");
            } try
            {
                Console.WriteLine("06/31/0001 : Expected Result? " + (DateParse("06/31/0001").ToString() == "6/31/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("06/31/0001 : Test Passed");
            } try
            {
                Console.WriteLine("09/31/0001 : Expected Result? " + (DateParse("09/31/0001").ToString() == "9/31/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("09/31/0001 : Test Passed");
            } try
            {
                Console.WriteLine("11/31/0001 : Expected Result? " + (DateParse("11/31/0001").ToString() == "11/31/0001 12:00:00 AM") + " Test Failed");
            }
            catch (Exception)
            {
                Console.WriteLine("11/31/0001 : Test Passed");
            }
            Console.ReadLine();
        }
    }
}
